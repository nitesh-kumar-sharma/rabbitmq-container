FROM rabbitmq:3
LABEL MAINTAINER="Nitesh K. Sharma <sharma.nitesh590@gmail.com>"

ENV PATH=$PATH:/opt/init/rabbitmq/

RUN rabbitmq-plugins enable --offline rabbitmq_management rabbitmq_mqtt rabbitmq_federation_management rabbitmq_stomp

ADD ./scripts/ /opt/init/rabbitmq/
RUN chmod -R +x /opt/init/rabbitmq/
EXPOSE 15671 15672 5672

ADD ./rabbitmq-entrypoint.sh /opt/init/rabbitmq/
RUN chmod -R +x /opt/init/rabbitmq/

ENTRYPOINT ["/bin/bash", "-C", "rabbitmq-entrypoint.sh","-d"]