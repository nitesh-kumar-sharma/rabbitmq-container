#!/bin/bash

initial-setup.sh

#For call the base entrypoint file so that rabbitmq don't break
docker-entrypoint.sh rabbitmq-server

if [[ -n $1 ]]; then
  echo "docker container started in continous running mode"
  echo "you can kill this terminal if you wish and can use docker exec -it <container_id> bash "
  echo "anytime."
  while true; do sleep 1000; done
else 
	exec $@;
fi;